'use strict';

var bump           = 0;
var lockeds        = [];
var selectedLocked = 0;
var first          = 0
var seconds        = [];
var thirds         = [];
var third          = 0;

$(() => {
  var collapsible = M.Collapsible.init(document.querySelector('.collapsible'));
  collapsible.destroy();
  $('#bump .incrementDisplay').text(bump.toFixed(1));
  $('#bump .incrementAdd').click(() => {
    if(bump >= 39.5)
      bump = 0;
    else
      bump += .5;
    $('#bump .incrementDisplay').text(bump.toFixed(1));
  });
  $('#bump .incrementSubtract').click(() => {
    if(bump <= 0)
      bump = 39.5;
    else
      bump -= .5;
    $('#bump .incrementDisplay').text(bump.toFixed(1));
  });
  $('#next').click(() => {
    //Get first digit from "bump" position.
    first = (Math.ceil(bump) + 5) % 40;
    //Get possible "locked" locations based on the first digit.
    for(var i = 0; i < 11; i++) {
      var thirdsTest = [];
      for(var j = 0; j < 4; j++) {
        var test = (10 * j + i);
        if(test < 40 && thirdsTest.indexOf(test) == -1 && test % 4 == first % 4)
          thirdsTest.push(test);
      }
      if(thirdsTest.length > 1 && lockeds.indexOf(i) == -1)
        lockeds.push(i);
    }
    lockeds.forEach(locked => $('#locked').append('<a class=\'btn-floating waves-effect waves-light red numberSelect lockedSelect\' onclick=selectedLocked=' + locked + '>' + locked + '</a>'));
    $('.lockedSelect').click(() => {
      //Get possible third digits based on selected locked position.
      for(var i = 0; i < 4; i++) {
        var test = (10 * i + selectedLocked);
        if(test < 40 && thirds.indexOf(test) == -1 && test % 4 == first % 4)
          thirds.push(test);
      }
      collapsible.open(2);
      collapsible.destroy();
      $('#third0').text(thirds[0]).click(() => third = thirds[0]);
      $('#third1').text(thirds[1]).click(() => third = thirds[1]);
      $('#third0, #third1').click(() => {
        //Get possible second digits once third digit is selected.
        for(var i = 0; i < 10; i++) {
          var test = ((first % 4 + 2) % 4) + 4 * i;
          if(((third + 2) % 40 != test && (third - 2) % 40 != test))
            seconds.push(test);
        }
        seconds.forEach(second => $('#combinations').append(first + ', ' + second + ', ' + third + '<br>'));
        collapsible.open(3);
        collapsible.destroy();
      });
    });
    collapsible.open(1);
    collapsible.destroy();
  });
});
