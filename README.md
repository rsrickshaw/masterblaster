# MasterBlaster
This tool is based on [Samy Kamkar's Algorithm](https://samy.pl/master/master.html) but has been modified to work in three steps instead of four and presents an easy to use, mobile-friendly, interface. It can reduce the 64,000 possible combinations down to a mere eight, allowing the lock to be opened in a matter of minutes.
